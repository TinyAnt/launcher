# WorldsOfCubes Launcher  

 - **Автор** : sashok724 and alexandrage (modified by WorldsOfCubes Group)
 - **Версия** : 1.0
 - **Сайт** : [WorldsOfCubes.NET](http://WorldsOfCubes.NET)
 - **Вики** : [bitbucket.org/WorldsOfCubes/launcher/wiki](http://bitbucket.org/WorldsOfCubes/launcher/wiki)

## Лицензия

 [WTFPL](http://wtfpl.net/about/) 
 
## Информация

### Содержимое WEB-части
* libraries.jar -> Библиотеки клиента.
* Forge.jar -> Minecraft Forge для 1.6+
* extra.jar -> Запасной джарник для OptiFine, PlayerApi, GuiApi и т.д.
* client.zip -> Содержит bin/natives для lwjgl, config для модов.  
* assets.zip -> Содержит в себе ресурсы для версий 1.6+