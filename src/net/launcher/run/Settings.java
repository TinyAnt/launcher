/*launcher, сборка за 30.10.2012, индекс: 121 */

package net.launcher.run;

public class Settings {
	/** Настройка заголовка лончера */
	public static final String 		title		 	 = "WorldsOfCubes | Лончер"; //Заголовок лончера
	public static final String 		titleInGame  	 = "WorldsOfCubes | Minecraft"; //Заголовок лончера после авторизации
	public static final String 		basedir			 = "AppData"; //Родительская папка для Minecraft (только для Windows) [ProgramFiles, AppData]
	public static final String 		baseconf		 = ".WorldsOfCubes"; //Папка с файлом конфигурации
	public static final String		pathconst		 = ".WorldsOfCubes/%SERVERNAME%"; //Конструктор пути к папке с MC
	public static final String      skins            = "MinecraftSkins/"; //Папка скинов
	public static final String      cloaks           = "MinecraftCloaks/"; //Папка плащей
	/** Параметры подключения */
	public static final String 	domain	 	 	 = "demo.worldsofcubes.net";//Домен сайта
	public static final String  siteDir		  	 = "MineCraft";//Папка с файлами лончера на сайте
	public static final String  updateFile		 = "http://demo.worldsofcubes.net/launcher/demo.jar";//Ссылка на файл обновления лончера
	public static final String 	buyVauncherLink  = "http://plati.ru/"; //Ссылка на страницу покупки ваучеров

	/** Для одиночной игры */
	public static final String  defaultUsername  = "Player"; //Имя пользователя для одиночной игры
	public static final String  defaultSession   = "123456"; //Номер сессии для одиночной игры
	
	public static int height                         = 532;      //Высота окна клиента
	public static int width                          = 900;      //Ширина окна клиента
	/** Настройка серверов */
	// 1-> Имя папки клиента 
	// 2-> ip 
	// 3-> port 
	// 4-> Версия клиента для автопатча директории (старые версии до 1.5.2)
	// 5-> Тип запуска клиента 1 для старых версий 2 для новых
	// 6-> 1 для запуска чистого клиента 1.6.2-1.6.4
	//     2 для запуска клиента с forge без Liteloader 1.6.2-1.6.4
	//     3 для запуска клиента с Liteloader и Liteloader+forge 1.6.2
        //     4 для запуска клиента с Liteloader и Liteloader+forge 1.6.4
	public static String[] servers =
	{
		"Test, 127.0.0.1, 25565, none, 2, 3",
	//	"NoRule, 46.174.48.165, 25565, none, 2, 3",
	//	"MineZ, 91.211.117.10, 25008, 1.5.x, 1, none",
	};

	/** Настройка панели ссылок **/
	public static final String[] links = 
	{
		//Для отключения добавьте в адрес ссылки #
		//" Регистрация ::http://",
	};

	/** Настройки структуры лончера */
	public static boolean useAutoenter			 =  true; //Использовать функцию автозахода на выбранный сервер
	
	public static boolean useRegister		 	 =  false; //Показывать предупреждение о том, что бета

	public static boolean useMulticlient		 =  true; //Использовать функцию "по клиенту на сервер"
	public static boolean useStandartWB			 =  true; //Использовать стандартный браузер для открытия ссылок
	public static boolean usePersonal		 	 =  false; //Использовать Личный кабинет
	public static boolean customframe 			 =  true; //Использовать кастомный фрейм
	public static boolean useOffline 			 =  true; //Использовать режим оффлайн
	public static boolean useConsoleHider		 =  true; //Использовать скрытие консоли клиента
	public static boolean useModCheckerTimer	 =  true; //Каждые 30 секунд моды будут перепроверяться

	public static final String protectionKey	 = "lalka"; //Ключ защиты сессии. Никому его не говорите.

	public static final boolean debug		 	 =  true;  //Отображать все действия лончера (отладка)(true/false)
	public static final boolean drawTracers		 =  false; //Отрисовывать границы элементов лончера
	public static final String masterVersion  	 = "final_R1"; //Версия лончера

	public static final boolean patchDir 		 =  true; //Использовать автоматическую замену директории игры (true/false)
	
	public static void onStart() {}
	public static void onStartMinecraft() {}
}